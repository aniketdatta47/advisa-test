export const TYPES = {
  SUM: 'sum',
  MULTIPLY: 'multiply'
};

export const ACTION_TYPES = {
  UPDATE_INPUT: 'UPDATE_INPUT',
  SET_TYPE: 'SET_TYPE'
};

export const inputOnChange = (id, evt, label) => {
  let value = evt.target.value.trim();
  let int = parseInt(value);
  if (!isNaN(int)) {
    value = int;
  } else {
    value = '';
  }

  return ({
    type: ACTION_TYPES.UPDATE_INPUT,
    value: value,
    id,
    label
  })
};

export const setVisibleType = visibleType => ({
  type: ACTION_TYPES.SET_TYPE,
  visibleType
});
