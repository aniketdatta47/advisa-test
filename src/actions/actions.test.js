import { setVisibleType, inputOnChange, ACTION_TYPES } from './index';

const MOCK_VISIBLE_TYPE = 'SOME_TYPE';
const MOCK_INPUT_CHANGE = {
  id: 1,
  evt: {
    target: {
      value: {
        trim: () => '2'
      }
    }
  },
  label: 'input_label'
};

describe('Actions', () => {
  it('returns valid setVisibleType', () => {
    const EXPECTED_RESULT = {
      type: ACTION_TYPES.SET_TYPE,
      visibleType: MOCK_VISIBLE_TYPE
    };

    expect(setVisibleType(MOCK_VISIBLE_TYPE)).toEqual(EXPECTED_RESULT);
  });

  it('returns valid inputOnChange', () => {
    const EXPECTED_RESULT = {
        type: ACTION_TYPES.UPDATE_INPUT,
        label: MOCK_INPUT_CHANGE.label,
        value: 2,
        id: MOCK_INPUT_CHANGE.id
    };

    expect(inputOnChange(MOCK_INPUT_CHANGE.id, MOCK_INPUT_CHANGE.evt, MOCK_INPUT_CHANGE.label)).toEqual(EXPECTED_RESULT);
  });
});
