import React, { Component } from 'react';
import ActionableSelect from './containers/ActionableSelect';
import InteractiveInputList from './containers/InteractiveInputList';
import ResultUpdater from './containers/ResultUpdater';
import { BACKGROUND_TYPES } from './components/Box';

import './App.css';
class App extends Component {
  render() {
    return (
        <div className={`app ${BACKGROUND_TYPES.GREY}`}>
          <InteractiveInputList></InteractiveInputList>
          <ActionableSelect>
            <ResultUpdater></ResultUpdater>
          </ActionableSelect>
        </div>
    );
  }
}

export default App;
