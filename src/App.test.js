import React from 'react';
import { mount, shallow } from 'enzyme';
import model from './reducers';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import App from './App';
import { BACKGROUND_TYPES } from './components/Box.js';
import ActionableSelect from './containers/ActionableSelect';
import InteractiveInputList from './containers/InteractiveInputList';
import ResultUpdater from './containers/ResultUpdater';

const store = createStore(model);

describe('App', () => {
  it('renders without crashing', () => {
    const wrapper = mount(<Provider store={store}><App /></Provider>);
    expect(wrapper.contains(ActionableSelect)).toBeTruthy();
    expect(wrapper.contains(InteractiveInputList)).toBeTruthy();
    expect(wrapper.contains(ResultUpdater)).toBeTruthy();
  });

  it('renders with configured background and Box', () => {
    const wrapper = shallow(<Provider store={store}><App /></Provider>);
    expect(wrapper.find(`.app ${BACKGROUND_TYPES.GREY}`)).toBeTruthy();
  });
});
