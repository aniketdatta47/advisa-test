import text, { DEFAULT_RESULT } from './results';
import { TYPES, ACTION_TYPES} from '../actions';
import inputs, { DEFAULT_INPUTS } from './inputsReducer';
import actionType from './actionType';

describe('Reducers', () => {
  describe('Text Reducer', () => {
    it('returns default text', () => {
      expect(text()).toEqual(DEFAULT_RESULT);
    });

    it('returns provided text', () => {
      const RESULT_TEXT = '12345';
      expect(text(RESULT_TEXT)).toEqual(RESULT_TEXT);
    });
  });

  describe('ActionType Reducer', () => {
    it('returns default actionType', () => {
      expect(actionType(TYPES.SUM, { type: 'default' }))
        .toEqual(TYPES.SUM);
    });

    it('returns set actionType', () => {
      expect(text(TYPES.MULTIPLY, { type: ACTION_TYPES.SET_TYPE }))
        .toEqual(TYPES.MULTIPLY);
    });
  });

  describe('Inputs Reducer', () => {
    it('returns default inputs', () => {
      expect(inputs(DEFAULT_INPUTS, {type: 'default'}))
        .toEqual(DEFAULT_INPUTS);
    });

    it('returns updated inputs', () => {
      const UPDATED_MOCK_INPUT = {
        id: 1,
        value: '2',
        label: 'some_label',
        type: ACTION_TYPES.UPDATE_INPUT
      };

      const UPDATED_INPUTS = DEFAULT_INPUTS;
      UPDATED_INPUTS[0].value = UPDATED_MOCK_INPUT.value;

      expect(inputs(DEFAULT_INPUTS, UPDATED_MOCK_INPUT))
        .toEqual(UPDATED_INPUTS);
    });
  });

});
