export const DEFAULT_INPUTS = [
  {id: 1, value: '0', label: 'Value 1'},
  {id: 2, value: '0', label: 'Value 2'},
  {id: 3, value: '0', label: 'Value 3'}
];

const inputs = (state = DEFAULT_INPUTS, action) => {
  switch (action.type) {
    case 'UPDATE_INPUT':
      if (action.id) {
        return state.map((input, index) => {
          if (input.id !== action.id) {
            return input;
          }

          if (action.value === '') {
            return {
              id: action.id,
              value: action.value,
              label: action.label
            };
          }

          return {
            ...input,
            value: `${action.value}`
          };
        })
      }

      return state;
    default:
      return state;
  }
}

export default inputs
