import { TYPES } from '../actions';

const actionType = (state = TYPES.SUM, action) => {
  switch (action.type) {
    case 'SET_TYPE':
      return action.visibleType
    default:
      return state
  }
}

export default actionType;
