import { combineReducers } from 'redux';
import inputs from './inputsReducer';
import actionType from './actionType';
import text from './results';

export default combineReducers({
  inputs,
  actionType,
  text
})
