import { connect } from 'react-redux';
import { setVisibleType } from '../actions';
import SelectInput from '../components/SelectInput';

const mapStateToProps = (state, ownProps) => ({
  actionType: state.actionType
});

const mapDispatchToProps = (dispatch) => ({
  onChange: (e) => dispatch(setVisibleType(e.target.value))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectInput);
