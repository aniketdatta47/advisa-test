import { connect } from 'react-redux';
import { inputOnChange } from '../actions';
import InputList from '../components/InputList';

const mapStateToProps = state => ({ inputs: state.inputs });
const mapDispatchToProps = dispatch => ({
  onChangeInput: (id, value) => dispatch(inputOnChange(id, value))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InputList);
