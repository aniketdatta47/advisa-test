import { connect } from 'react-redux';
import Result from '../components/Result';
import { TYPES } from '../actions';
import { DEFAULT_RESULT } from '../reducers/results';

const mapStateToProps = (state, ownProps) => {
		let result = state.actionType === TYPES.SUM ? 0 : 1;

		state.inputs.forEach((input) =>
			state.actionType === TYPES.SUM ?
				result += parseInt(input.value) :
				result *= parseInt(input.value)
		);

		return {
			text: !isNaN(result) ? `${result}` : DEFAULT_RESULT
		};
}

export default connect(
	mapStateToProps
)(Result);
