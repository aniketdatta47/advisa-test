import React from 'react';
import { shallow } from 'enzyme';
import ValueInput from './ValueInput';
import Box, { BACKGROUND_TYPES } from './Box';

const mockOnChange = jest.fn();
const LABEL_TEXT = 'button_label';
const DEFAULT_VALUE = '0';

describe('ValueInput', () => {
  it('renders 2 radio inputs with label within a box', () => {
    const wrapper = shallow(
      <ValueInput onChange={mockOnChange} value={DEFAULT_VALUE} label={LABEL_TEXT}  />
    );

    expect(wrapper.find(Box).length).toEqual(1);
    expect(wrapper.find(Box).props().backgroundType).toEqual(BACKGROUND_TYPES.BLUE);
    expect(wrapper.find(Box).props().inputBox).toEqual(true);

    expect(wrapper.find('label > input').length).toEqual(1);
    expect(wrapper.find('label').text()).toEqual(`${LABEL_TEXT}: `);
    expect(wrapper.find('input').props().type).toEqual('text');
    expect(wrapper.find('input').props().value).toEqual(DEFAULT_VALUE);
  });


  it('triggers custom onChange handler onChange', () => {
    const wrapper = shallow(
      <ValueInput onChange={mockOnChange} value={DEFAULT_VALUE} label={LABEL_TEXT} />
    );

    wrapper.find('input').at(0).simulate('change');
    wrapper.find('input').at(0).simulate('change');

    expect(mockOnChange.mock.calls.length).toEqual(2);
  });
});
