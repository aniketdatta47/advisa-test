import React from 'react';
import PropTypes from 'prop-types';
import Box, { BACKGROUND_TYPES } from './Box';
import './ValueInput.css';

const ValueInput = ({ onChange, value, label }) => (
  <Box backgroundType={BACKGROUND_TYPES.BLUE} inputBox>
    <label>
      {`${label}: `}
      <input
        className='value'
        type='text'
        value={value}
        onChange={onChange}
      />
    </label>
  </Box>
)

ValueInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
}

export default ValueInput
