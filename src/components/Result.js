import React from 'react';
import PropTypes from 'prop-types';
import './Box.css';

const Result = ({ text }) => (
  <p className='result'>
    Result: {text}
  </p>
)
Result.propTypes = {
  text: PropTypes.string.isRequired
}

export default Result;
