import React from 'react';
import { shallow } from 'enzyme';
import Result from './Result';

const SOME_TEXT = 'text';

describe('Result', () => {
  it('renders with provided text', () => {
    const wrapper = shallow(
      <Result text={SOME_TEXT} />
    );
    expect(wrapper.find('.result').length).toEqual(1);
    expect(wrapper.find('.result').text()).toEqual(`Result: ${SOME_TEXT}`);
  });
});
