import React from 'react';
import { shallow } from 'enzyme';
import SelectInput from './SelectInput';
import Box from "./Box";
import { TYPES } from '../actions';

const mockOnChange = jest.fn();

describe('SelectInput', () => {
  it('renders 2 radio inputs with label within a box', () => {
    const wrapper = shallow(
      <SelectInput onChange={mockOnChange} actionType={TYPES.SUM} />
    );

    expect(wrapper.find(Box).length).toEqual(1);
    expect(wrapper.find('p.input > label > input').length).toEqual(2);
    expect(wrapper.find('p > label').at(0).text()).toEqual("Sum");
    expect(wrapper.find('p > label').at(1).text()).toEqual("Multiply");
  });

  it('renders input checked based on actionType', () => {
    const wrapper = shallow(
      <SelectInput onChange={mockOnChange} actionType={TYPES.SUM} />
    );

    expect(wrapper.find('input').at(0).props().checked).toEqual(true);
  });

  it('triggers custom onChange handler onChange', () => {
    const wrapper = shallow(
      <SelectInput onChange={mockOnChange} actionType={TYPES.SUM} />
    );

    wrapper.find('input').at(0).simulate("change");
    wrapper.find('input').at(1).simulate("change");
    wrapper.find('input').at(0).simulate("change");

    expect(mockOnChange.mock.calls.length).toEqual(3);
  });
});
