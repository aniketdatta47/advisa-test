import React from 'react';
import PropTypes from 'prop-types';
import './Box.css';

export const BACKGROUND_TYPES = {
  GREY: 'grey',
  BLUE: 'blue',
  RED: 'red'
};

const Box = ({ children, backgroundType, inputBox }) => (
  <div className={`container ${backgroundType} ${ inputBox ? 'inputBox' : 'resultsBox'}`}>
    {children}
  </div>
);

Box.propTypes = {
  backgroundType: PropTypes.string.isRequired
}

export default Box;
