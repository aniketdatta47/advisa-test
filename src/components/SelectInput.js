import React from 'react';
import PropTypes from 'prop-types';
import { TYPES } from '../actions';
import Box, { BACKGROUND_TYPES } from './Box';
import './Box.css';

const Select = ({ children, actionType, onChange }) => (
  <Box backgroundType={BACKGROUND_TYPES.RED}>
    <p className='input'>
      <label>
        <input
          type='radio'
          name='Sum'
          checked={actionType === TYPES.SUM}
          value={TYPES.SUM}
          onChange={onChange} />
        Sum
      </label>
    </p>
    <p className='input'>
      <label>
        <input
          type='radio'
          name='Multiply'
          checked={actionType === TYPES.MULTIPLY}
          value={TYPES.MULTIPLY}
          onChange={onChange} />
        Multiply
      </label>
    </p>
    {children}
  </Box>
);

Select.propTypes = {
  actionType: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Select
