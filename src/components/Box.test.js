import React from 'react';
import { mount } from 'enzyme';
import Box, { BACKGROUND_TYPES } from './Box';

describe('Box', () => {
  it('renders default with resultsBox class', () => {
    const wrapper = mount(
      <Box backgroundType={BACKGROUND_TYPES.GREY}/>
    );
    expect(wrapper.find('.container').length).toEqual(1);
    expect(wrapper.find(`.${BACKGROUND_TYPES.GREY}`).length).toEqual(1);
    expect(wrapper.find('.resultsBox').length).toEqual(1);
  });

  it('should render a child', () => {
    const child = <div>1</div>;
    const wrapper = mount(
      <Box backgroundType={BACKGROUND_TYPES.GREY} children={child} />
    );
    expect(wrapper.children().length).toEqual(1);
  });

  it('should render a Box with inputBox class', () => {
    const wrapper = mount(
      <Box backgroundType={BACKGROUND_TYPES.GREY} inputBox />
    );
    expect(wrapper.find('.inputBox').length).toEqual(1);
  });
});
