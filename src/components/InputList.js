import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ValueInput from './ValueInput';

const InputList = ({ inputs, onChangeInput }) => (
  <Fragment>
    {inputs.map(input =>
      <ValueInput
        key={input.id}
        {...input}
        onChange={(e) => onChangeInput(input.id, e, input.label)}
      />
    )}
  </Fragment>
)

InputList.propTypes = {
  inputs: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  }).isRequired).isRequired,
  onChangeInput: PropTypes.func.isRequired
}

export default InputList
