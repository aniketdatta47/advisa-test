This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Design Choices

## Create React App, React, redux
  Create React App helps create a easy setup really quickly. Used React since I task contained several similar components (for a modular structure) and would be a good case for using React's DOM updater.

  Redux, since I really wanted to learn something new and challenge myself in completing a functional app using Redux. This is my first time using Redux, generally have used different kinds of custom PUB/SUB events with an Observable pattern. Redux really forced me to seperate component rendering, from the update functionality (presentational logic) and data model logic (business logic).

### components/
  Contains only renderable html components.

### containers/
  Provides the interactable logic to our library of components. This helps create specific custom components, while being able to craft other ones with the help of components/

### actions
  Definition of app actions.

### reducers
  Contains model logic of how inputs get updated, what text is displayed and for handling SUM or MULTIPLY

### app
  Our application composition of components (this at a high-level) should describe what the app does.

### index
  Sets up root level dependencies for app, slightly different abstraction from app (since that handles the rendering structure) while this initialises the models and other things (serviceWorkers), maybe even other services (to fetch data externally).

## Colours and Layout

The colours used are from https://material.io/design/color/the-color-system.html#tools-for-picking-colors, to match the design mock up.

Responsive layout is achieved with use of Flexbox. Sizing is with use of em units (mainly due to the simplicity of the app), Generally I would avoid em units for layout, as they can become complicated when thinking of sizes based on font size. While fonts can be sized with em, I would try to avoid it with layout for complex layouts.

Added a slight border to the input (when on focus), to help signify active input. Inputs are nested within labels so that on clicking the label, the input is focussed (really useful for radio type inputs).


## Further improvements:
- Would create a generic InputWithLabel, which would encapsulate the nested input within label - to handle the label being on the left/right side of the input. This would then be used by ValueInput, and SelectInput (to render different kinds of inputs).

- Using css modules and classnames for better CSS encapsulation. Better abstraction of css based on components/, currently most of it is in Box.css.

- Better redux tests by mocking the store, dispatch and action methods

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
